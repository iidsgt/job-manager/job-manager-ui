import React, { Component } from 'react'
import {Navbar, Classes, NavbarGroup, Alignment, Button, AnchorButton} from "@blueprintjs/core"
import checkAuthentication from '../Utils/Auth'
import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import {Link} from "react-router-dom"

export default class MenuBar extends Component {

    componentWillMount() {
        checkAuthentication()
    }

    render() {
        return (
            <Navbar className={Classes.DARK}>
                    <NavbarGroup align={Alignment.LEFT}>
                        <Link to="/">
                        <Button
                            text="Home"
                            minimal
                            rightIcon="home"
                            /></Link>
                        <Link to="/jobs">
                        <Button
                            text="Jobs"
                            minimal
                            rightIcon="dashboard"
                            /></Link>
                        <Link to="/data">
                        <Button
                            text="Data"
                            minimal
                            rightIcon="share"
                            /></Link>
                        
                    </NavbarGroup>
                    <NavbarGroup align={Alignment.RIGHT}>
                    <AnchorButton
                            minimal
                            rightIcon="log-out"
                            href="/logout"
                        />
                        
                    </NavbarGroup>
                </Navbar>
        )
    }
}


