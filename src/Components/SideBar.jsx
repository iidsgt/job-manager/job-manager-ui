import React, { Component } from 'react'
import {Navbar, Classes, NavbarGroup, Alignment, Button, AnchorButton, Menu, MenuItem} from "@blueprintjs/core"
import checkAuthentication from '../Utils/Auth'
import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import {Link} from "react-router-dom"

export default class SideBar extends Component {

    componentWillMount() {
        checkAuthentication()
    }

    render() {
        return (
            <div className="menu-sidebar">
                <h4 className="bp3-heading header-text">Pages</h4>
                <Link to="/jobs">
                <Button minimal={true} className="button-text">Job Central</Button>
                </Link>
                <Link to="/data">
                <Button minimal={true} className="button-text">Data Central</Button>
                </Link>
            </div>
        )
    }
}


