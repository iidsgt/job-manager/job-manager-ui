import React, { Component } from 'react'
import { Route, Redirect } from "react-router"



//Route Definitions

/*export const PrivateRoute = ({ component: Component, ...rest }) => (

    <Route {...rest} render={(props) => (
        props.isAuthenticated === true
        ? <Component {...props} />
        : <Redirect to={{
            pathname: "/login",
            state: { from: props.location }
          }} />
    )} />
  )*/

export const PrivateRoute = ({ component: Component, isAuthenticated:isAuthenticated, ...rest }) => {
        return (
            <Route {...rest} render={props => {
                if (isAuthenticated() === true) {
                    return <Component {...props}/>
                } else {
                    return (
                        <Redirect
                            to="/login"
                        />
                    )
                }

            }} />
        )
}