import React from 'react';
import {LoginPage} from './Pages/Login'
import {MainPage} from './Pages/MainPage'
import {LogoutPage} from './Pages/LogoutPage'
import {JobsPage} from './Pages/JobsPage'
import {PrivateRoute} from './Components/PrivateRoutes'
import { BrowserRouter as Router, Route, Link } from "react-router-dom"
import './App.css';


function isAuthenticated() {
  if (localStorage.getItem("xxjmAuthorization")) {
    return true
  } else {
    return false
  }
}

function App() {
  
  return (
    <div className="App">
      <Router>
        <Route path="/login" exact component={LoginPage} />
        <PrivateRoute path="/" exact component={MainPage} isAuthenticated={isAuthenticated}/>
        <PrivateRoute path="/jobs" exact component={JobsPage} isAuthenticated={isAuthenticated}/>
        <Route path="/logout" exact component={LogoutPage}/>
      </Router>
      
    </div>
  );
}

export default App;
