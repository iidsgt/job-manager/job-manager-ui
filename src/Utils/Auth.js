

export default async function checkAuthentication() {
    try {
        let response = await fetch('http://localhost:8080/v1/auth/verify', {
            headers: {
                'Authorization':'Bearer ' +  localStorage.getItem("xxjmAuthorization")
            }
        })
        if (await response.ok === true) {
            return true
            
        } else {
            console.log("problem")
            localStorage.removeItem("xxjmAuthorization")
            window.location = "/login"
            return false
        }
    } catch(e) {
        console.log("problem")
        localStorage.removeItem("xxjmAuthorization")
        window.location = "/login"
        return false
    }
}