import React, { Component } from 'react'
import {Classes, Alignment, Button, AnchorButton, Card, FileInput, Dialog} from "@blueprintjs/core"
import {Select} from "@blueprintjs/select"
import checkAuthentication from '../Utils/Auth'
import MenuBar from "../Components/MenuBar"
import SideBar from "../Components/SideBar"
import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";

export class MainPage extends Component {
    constructor() {
        super();
        this.state = {
            filelabels: {
                cwl: "Select CWL",
                inputs: "Select Inputs",
                mapping: "Select Mapping"
            },
            formdata: {
                cwl: null,
                inputs: null,
            },
            overlay: {
                isopen: false,
                text: ""
            }
        }
    }

    componentWillMount() {
        checkAuthentication()
    }

    async submit() {
        if (this.state.formdata.cwl !== null && this.state.formdata.inputs !== null && this.state.formdata.mapping !== null) {
            let data = new FormData()
            data.append('workflow', this.state.formdata.cwl)
            data.append('inputs', this.state.formdata.inputs)
            //data.append('mappings', this.state.formdata.mapping)

            let response = await fetch('http://localhost:8080/v1/jobs/submitjob', {
                method: "POST",
                headers: {
                    Authorization: 'Bearer ' +  localStorage.getItem("xxjmAuthorization")
                },
                body: data

            })
            if (response.ok) {
                let s = this.state
                s.overlay.isopen = true
                s.overlay.text = "Submitted!"
                this.setState(s)
            } else {
                let s = this.state
                s.overlay.isopen = true
                s.overlay.text = "Problem"
                this.setState(s)
            }
        } else {
            let s = this.state
                s.overlay.isopen = true
                s.overlay.text = "Problem"
                this.setState(s)
        }
    }

    handleFiles(e, name) {

        console.log(name)
        switch (name) {
            case "cwl": {
                let s = this.state
                s.filelabels.cwl = e.target.files[0].name
                s.formdata.cwl = e.target.files[0]
                this.setState(s)
                break
            }
            case "inputs": {
                let s = this.state
                s.filelabels.inputs = e.target.files[0].name
                s.formdata.inputs = e.target.files[0]
                this.setState(s)
                break
            }
            case "mapping": {
                console.log(e.target.files)
                let s = this.state
                s.filelabels.mapping = e.target.files[0].name
                s.formdata.mapping = e.target.files[0]
                this.setState(s)
                break
            }
        }
    }

    render() {
        return (
            <div>
            <MenuBar/>
            <Dialog isOpen={this.state.overlay.isopen} onClose={() => this.setState({overlay:{text:"", isopen:false}})}>
                <h2>{this.state.overlay.text}</h2>
            </Dialog>
            <div className="mainpage-holder">
                <SideBar />
                <div className="content-holder">
                    <Card id="circle-graph-card">
                        <h3>Hi</h3>
                    </Card>
                </div>
            </div>
            {/*<Card className="submission-card">
                <h3>Submit Your Job</h3>
                <FileInput text={this.state.filelabels.cwl} className="fileinput" onInputChange={(e) => this.handleFiles(e, "cwl")}/>

                <br/>
                <FileInput text={this.state.filelabels.inputs} className="fileinput" onInputChange={(e) => this.handleFiles(e, "inputs")}/>
                <br/>
        {/*<FileInput text={this.state.filelabels.mapping} className="fileinput" onInputChange={(e) => this.handleFiles(e, "mapping")}/>
                <br/>}
                <Button text="Submit Workflow" intent="primary" onClick={this.submit.bind(this)} onKeyDown={this.submit.bind(this)}/>
            </Card>*/}
            </div>
        )
    }
}