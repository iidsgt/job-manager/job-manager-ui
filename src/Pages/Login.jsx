import React, { Component } from 'react'
import {FormGroup, Classes, Card, Button, Alert} from "@blueprintjs/core"
import {Redirect} from 'react-router-dom'
import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";

export class LoginPage extends Component {
    constructor() {
        super();
        this.state = {
            username:"",
            password:"",
            redirectToRefer:false,
            overlay: {
                isopen: false
            }
        }
    }

   async submit(e) {
       //console.log(e)
        e.preventDefault()
        try {
            let response = await fetch('http://localhost:8080/v1/auth', {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({username:this.state.username, password:this.state.password})
            })
            if (response.ok === true) {
                let p = await response.json()
                window.localStorage.setItem("xxjmAuthorization", p.access_token)
                let s = this.state
                s.redirectToRefer = true
                this.setState(s)
            } else {
                let s = this.state
                s.overlay.isopen = true
                this.setState(s)
            }
             
        } catch (e) {
            let s = this.state
            s.overlay.isopen = true
            this.setState(s)
        }
    }

    handleKey(e) {
        switch(e.key) {
            case 'Enter' : {
                this.submit(e)
            }
        }
    }

    handleChange(e) {
        switch(e.target.name) {
            case 'default' : {
                return true
            }
            case 'username' : {
                let s = this.state
                s.username = e.target.value
                this.setState(s)
            }

            case 'password' : {
                let s = this.state
                s.password = e.target.value
                this.setState(s)
            }
        }
    }

    render() {
        let { from } = this.props.location.state || { from: { pathname: "/" } };
        if (this.state.redirectToRefer) {
            return <Redirect to="/"/>
        }
        return (
            <>
                <Alert isOpen={this.state.overlay.isopen} onClose={() => this.setState({overlay:{isopen:false}})}>
                    <h3>Username or Password Incorrect</h3>
                </Alert>
                <Card className="login-card">
                    <h3>Login To Job Manager</h3>
                <FormGroup>
                    <div className="bp3-input-group">
                    <input name="username" className="bp3-input file-input" placeholder="Username" onChange={this.handleChange.bind(this)}/>
                    <span className="bp3-button bp3-minimal bp3-intent-warning bp3-icon-user"></span>
                    </div>
                    <span></span>
                    <div className="bp3-input-group">
                    <input type="password" name="password" className="bp3-input" placeholder="Password" onChange={this.handleChange.bind(this)} onKeyDown={this.handleKey.bind(this)}/>
                    <span className="bp3-button bp3-minimal bp3-intent-warning bp3-icon-lock"></span>
                    </div>
                    <Button text="Login" intent="primary" onClick={this.submit.bind(this)} onKeyDown={this.submit.bind(this)}/>
                </FormGroup>
                </Card>
            </>
        )
    }
}