import React, { Component } from 'react'
import {Classes, Alignment, Button, AnchorButton, Divider, Card, Dialog} from "@blueprintjs/core"
import {Select} from "@blueprintjs/select"
import SyntaxHighlighter from 'react-syntax-highlighter';
import { atomOneLight } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import {default as yaml} from 'js-yaml'
import checkAuthentication from '../Utils/Auth'
import MenuBar from "../Components/MenuBar"
import SideBar from "../Components/SideBar"
import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";


export class JobsPage extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            overlay: {
                isopen: false,
                cwl: null,
                inputs: null,
                mapping: null,
                job_id: null
            }
        }
        this.jobinterval = null
    }

    async componentWillMount() {
        checkAuthentication()
        this.updateJobs()
    }

    componentWillUnmount() {
        clearInterval(this.jobinterval)
    }

    componentDidMount() {
        this.jobinterval = setInterval(this.updateJobs.bind(this), (9 * 1000));
    }

    async updateJobs() {
        let response = await fetch('http://localhost:8080/v1/jobs', {
            headers: {
                Authorization: 'Bearer ' +  localStorage.getItem("xxjmAuthorization")
            }
        })

        if (response.ok === true) {
            let p = await response.json()
            let s = this.state
            s.data = p
            this.setState(s)
        }
    }

    openOverlay(cwl, inputs, mapping, job_id, status) {
        if (this.state.overlay.isopen === true) {
            let s = this.state
            s.overlay.isopen = false
            this.setState(s)
        } else {

        
        let s = this.state
        s.overlay.isopen = true
        s.overlay.cwl = cwl
        s.overlay.inputs = inputs
        s.overlay.mapping = mapping
        s.overlay.job_id = job_id
        s.overlay.status = status
        this.setState(s)
        }
    }


    render() {
        let comp = (
            <div className="jobinfo">
                <h3>Job ID:</h3><span>{this.state.overlay.job_id}</span>
                <h3>Status:</h3><span>{this.state.overlay.status}</span>
                <h3>CWL</h3>
                <SyntaxHighlighter language="yaml" className="codeholder" style={atomOneLight}>
                    {yaml.safeDump(this.state.overlay.cwl)}
                </SyntaxHighlighter>
                <h3>Inputs</h3>
                <SyntaxHighlighter language="json" className="codeholder" style={atomOneLight}>
                    {JSON.stringify(this.state.overlay.inputs, null, 2)}
                </SyntaxHighlighter>
                <h3>File Mappings</h3>
                <SyntaxHighlighter language="json" className="codeholder" style={atomOneLight}>
                    {JSON.stringify(this.state.overlay.mapping, null, 2)}
                </SyntaxHighlighter>
            </div>
        )

        return (
            <div>
            <MenuBar/>
            <SideBar/>
            <div className="jobpageholder">
            {this.state.overlay.isopen ? comp : null }
            {/*
            <Dialog isOpen={this.state.overlay.isopen} onClose={() => this.openOverlay()}>
                        <Card>
                            <div>
                                <h3>Job ID:</h3><span>{this.state.overlay.job_id}</span>
                                <h3>CWL</h3>
                            <SyntaxHighlighter language="yaml" className="codeholder" style={atomOneLight}>
                                {yaml.safeDump(this.state.overlay.cwl)}
                            </SyntaxHighlighter>
                            <h3>Inputs</h3>
                            <SyntaxHighlighter language="json" className="codeholder" style={atomOneLight}>
                                {JSON.stringify(this.state.overlay.inputs, null, 2)}
                            </SyntaxHighlighter>
                            <h3>File Mappings</h3>
                            <SyntaxHighlighter language="json" className="codeholder" style={atomOneLight}>
                                {JSON.stringify(this.state.overlay.mapping, null, 2)}
                            </SyntaxHighlighter>
                            </div>
                        </Card>
                        <Button intent="warning" fill="false" text="ReRun" icon="refresh"/>
            </Dialog>*/}

            <div className="jobholder">
            {this.state.data.map((data, i) => {
                let rowColorClass = ["jobwrapper"]
                switch (data.status) {
                    case 'Succeeded': {
                        rowColorClass.push("succeeded")
                        break;
                    }
                    case 'Failed': {
                        rowColorClass.push('failed')
                        break;
                    }
                    default: {
                        break;
                    }
                }
                return (
                    
                    <div key={i} className={rowColorClass.join(" ")} onClick={() => this.openOverlay(data.cwl, data.inputs, data.file_mapping, data.job_id, data.status)}>
                        <h4>{i + 1}</h4>
                        <Divider/>
                        <h4>Created At: {new Date(data.created_at * 1000).toLocaleDateString()} {new Date(data.created_at * 1000).toLocaleTimeString()}</h4>
                        <Divider/>
                            <h4>Status: {data.status}</h4>
                    </div>
                    
                )
            })}
            </div>
            </div>
            </div>
        )
    }
}