import React, { Component } from 'react'
import {Navbar, Classes, NavbarGroup, Alignment, AnchorButton} from "@blueprintjs/core"
import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";

export class LogoutPage extends Component {


    render() {
        let redirect = () => {
            window.location = "/"
        }
        localStorage.removeItem("xxjmAuthorization")
        setTimeout(redirect, 3 * 1000)
        return (
                <span>Sorry to see you go</span>
        )
    }
}