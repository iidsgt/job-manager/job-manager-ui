// Original card stack for jobs

<Card key={i} interactive={true} className={rowColorClass} onClick={() => this.openOverlay(data.cwl, data.inputs, data.file_mapping, data.job_id)}>
                        <h4>{i + 1}</h4>
                        <Divider/>
                        <h4>Created At: {new Date(data.created_at * 1000).toLocaleDateString()} {new Date(data.created_at * 1000).toLocaleTimeString()}</h4>
                        <Divider/>
                            <h4>Status: {data.status}</h4>
                    </Card>
                    